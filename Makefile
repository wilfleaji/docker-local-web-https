install:
	bash ./scripts/init.sh && docker-compose build --no-cache
run:
	docker-compose up -d

.PHONY: install run
