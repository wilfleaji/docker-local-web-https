#!/bin/bash

openssl genrsa -des3 -passout pass:toto -out rootCA.key 2048

openssl req -x509 -new -nodes -passin pass:toto -key rootCA.key -sha256 -days 1024 -out /alternate-root/rootCA.pem -subj "/C=FR/ST=france/L=nt/O=nv/OU=it Department/CN=mysitename.local"

openssl req -new -sha256 -nodes -out server.csr -newkey rsa:2048 -keyout /alternate-root/server.key -config <( cat /alternate-root/server.csr.cnf )

openssl x509 -req -in server.csr -CA /alternate-root/rootCA.pem -CAkey rootCA.key -CAcreateserial -out /alternate-root/server.crt -days 500 -sha256 -passin pass:toto -extfile /alternate-root/v3.ext
