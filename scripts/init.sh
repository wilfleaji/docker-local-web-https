#!/bin/bash

echo site url :

read url

echo project name :
read name

echo local source :
read src

TEMPLATE="srvconf/template.conf"

if [ -f $TEMPLATE ]; then
    mv srvconf/template.conf srvconf/$url.'conf'
    sed -i "s/mysitename.local/$url/g" ./srvconf/$url.'conf'
    sed -i "s/mysitename.local/$url/g" Dockerfile
    sed -i "s/mysitename.local/$url/g" ./srvconf/server.csr.cnf
    sed -i "s/mysitename.local/$url/g" ./srvconf/v3.ext
    sed -i "s/sitename/$name/g" docker-compose.yml
    sed -i "s=sitepath=$src=g" docker-compose.yml
    sed -i "s/mysitename.local/$url/g" ./scripts/generatessl.sh
    
else
    echo site already enabled
fi
