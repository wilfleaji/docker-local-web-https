FROM wordpress:php7.0-apache

RUN mkdir /alternate-root

COPY srvconf/server.csr.cnf /alternate-root/server.csr.cnf
COPY srvconf/v3.ext /alternate-root/v3.ext
COPY scripts/generatessl.sh /alternate-root/generatessl.sh
COPY scripts/entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

RUN apt-get update && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y openssl vim nodejs sendmail && \ 
    bash /alternate-root/generatessl.sh

RUN mkdir -p /etc/ssl/localcerts && \
    cp /alternate-root/server.crt /etc/ssl/localcerts && \
    cp /alternate-root/server.key /etc/ssl/localcerts

ADD php.ini /usr/local/etc/php/conf.d/app.ini

RUN a2enmod rewrite
RUN a2enmod ssl
RUN service apache2 restart

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]